<?php

echo "--------------------------------------------------------------------------<br><br>";

echo "Task #1: <br>";

/**
 * Prints the given string letter by letter
 * @param string String which should be splitted letter by letter 
 * @return void
 */
function literuj($str): void
{
    $tmp = mb_str_split($str);
    for ($i = 0; $i < mb_strlen($str); $i++) {
        echo $i, ' - ', $tmp[$i], PHP_EOL;
    }
}
literuj('lorem ipsum dolor sit amet'); #wywołanie 1
literuj('zażółć gęślą jaźń'); # wywołanie 2

echo "<br>The problem was related with charset. As I know php uses ISO-8859 encoding by default. 
      Second example is in Polish language, so for that
      we need to use UTF8 charset and string functions which starts with allias <b>mb_</b>  (for multibyte string): 
      <br> --------------------------------------------------------------------------<br><br>";

echo "Task #2: <br>";

/**
 * Prints product id, name and price according to the second parameter
 * @param array $product list of products
 * @param array $arr list of desired products which should be found in $product
 * @return void
 */
function printProduct($product, $arr): void
{
    $positions = [];

    foreach ($product as $key => $value) {
        $positions[$value['id']] = $key;
    }

    foreach ($arr as $value) {
        $position = $positions[$value['result_id']];
        echo 'id: ', $product[$position]['id'], ', name: ', $product[$position]['name'], ', price: ', $product[$position]['price'], '<br>';
    }
}
$array = [
    ["id" => 123, "name" => "test0", "price" => 10],
    ["id" => 432, "name" => "test1", "price" => 20],
    ["id" => 231, "name" => "test2", "price" => 30],
    ["id" => 342, "name" => "test3", "price" => 10]
];

$results = [
    ["result_id" => 432],
    ["result_id" => 342]
];


printProduct($array, $results);

echo "--------------------------------------------------------------------------<br><br>";


echo "Task #3: <br><br>";
/**
 * @param array $arr main tree
 * @param int $parrent the nested level, default 1
 * @return void
 */
function printCategory($arr, $parrent = 1): void
{
    foreach ($arr as $value) {
        $level = $parrent;
        echo "category_id: $value[id] level: $level <br>";
        if (isset($value['subcats'])) {
            printCategory($value['subcats'], $parrent + 1);
        }
    }
}
$cats = [
    ['id' => 1],
    ['id' => 2],
    [
        'id' => 3,
        'subcats' => [
            ['id' => 31],
            [
                'id' => 32,
                'subcats' => [
                    ['id' => 321],
                    ['id' => 322],
                ]
            ],
        ]
    ],
    ['id' => 4],
];

printCategory($cats);

echo "--------------------------------------------------------------------------";


